# Test hello CI-CD

This project shows how to use SAAGIE's gradle pluggin for CI-CD. 

## Usage
Fork this project in your space and modify it accordingly (here it is the simplest structure thus only the hello_world.py file is used).

## Organisation of the files
Most of the file are here as empty example of what information is needed. What is really used is : 

code
  - hello_world.py

saagie
  - jobs
      - build.gradle
      - gradle-process.properties

.gitlab-ci.yml

## Update the `gradle-process.properties` script with the right parameters
You will find the ids needed in the url to the job concerned. E.g with

`https://an_url_to_the_plateform.com/projects/platform/1/project/XXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/job/XXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/instances`

You get the following informations depending in which environment you are in :
  * env = 1 ; it is the number given after platform
  * saagieProjectIdDev = XXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX ; what follows project
  * saagieProjectIdProd
  * saagieJobIdDev = XXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX ; what follows job
  * saagieJobIdProd

Since there only one platform there is no prod ids. The variable `env` is moreover not used and set to 1 in `build.gradle`. All the other variables are given in the `gradle-process.properties`.

## Setup Gitlab variables
In the Variables section of `settings/ci_cd` of your Gitlab project set the `cicd_id` and `cicd_pw` variables (protect and mask the password) to allow access to Gitlab to your process. You can see the used in `build.gradle`.

## Deploy
If you want to update the code and the job, run the following command in the folder /saagie/jobs:  
`gradle projectsUpdateJob  -PjobName=process -Penv=dev` to update the job  
`gradle projectsRunJob -PjobName=process -Penv=dev` to update and run the job  
  
## CI / CD in gitlab:  
When you push your code in gitlab, .gitlab-ci.yml file is configured in the root folder of the projet.
It will run the gradle plugin, update and run the code. You may check the result in the instance logs of the job.